from tqdm import tqdm
import time


class BarCounter:
    def __init__(self, delay: float = 0.1) -> None:
        self._delay = delay

    def count(self, n: int) -> None:
        for _ in tqdm(range(n), ncols=80, colour="green"):
            time.sleep(self._delay)


if __name__ == "__main__":
    BarCounter().count(100)
