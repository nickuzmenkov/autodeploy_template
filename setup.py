from setuptools import setup

with open("README.md") as file:
    long_description = file.read()

setup(
    name="autodeploy_template",
    packages=["package"],
    version="0.1.0",
    license="MIT",
    description="This is a template package to show CI/CD with BitBucket pipelines",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Nick Kuzmenkov",
    author_email="nickuzmenkov@yahoo.com",
    url="https://bitbucket.org/nickuzmenkov/autodeploy_template.git",
    keywords=["python"],
    install_requires=["tqdm>=4.62.0"],
)
